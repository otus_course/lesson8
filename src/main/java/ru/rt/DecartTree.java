package ru.rt;

import java.util.Random;

public class DecartTree extends SimpleTree{
    private Random rand = new Random();

    public Node merge(Node L, Node R) {
        if (L == null) return R;
        if (R == null) return L;

        if (L.height > R.height) {
            Node newR = merge(L.right, R);
            return new Node(L.key, L.height, L.left, newR);
        } else {
            Node newL = merge(L, R.left);
            return new Node(R.key, R.height, newL, R.right);
        }
    }

    private SplitResult split(int x, Node current) {
        Node L = null;
        Node R = null;
        Node newTree  = null;
        if (current == null) {
            return new SplitResult(L, R);
        }
        if (current.key <= x) {
            if (current.right == null) {
                R = null;
            } else {
                SplitResult split = split(x, current.right);
                newTree = split.left;
                R = split.right;
            }
            L = new Node(current.key, current.height, current.left, newTree);
        } else {
            if (current.left == null) {
                L = null;
            } else {
                SplitResult split = split(x, current.left);
                L = split.left;
                newTree = split.right;
            }
            R = new Node(current.key, current.height, newTree, current.right);
        }

        return new SplitResult(L, R);
    }

    @Override
    public void insert(int x) {
        if(root == null){
            root = new Node(x, rand.nextInt());
        }else {
            Node l, r;
            SplitResult split = split(x, root);
            l = split.left;
            r = split.right;
            Node m = new Node(x, rand.nextInt());
            root = merge(merge(l, m), r);
        }
        this.size++;
    }

    @Override
    public void remove(int x) {
        Node l, r;
        SplitResult split = split(x - 1,root);
        l = split.left;
        r = split.right;

        split = split(x, r);
        r = split.right;
        root = merge(l, r);
        this.size--;
    }

    private static class SplitResult {
        Node left;
        Node right;

        public SplitResult(Node left, Node right) {
            this.left = left;
            this.right = right;
        }
    }
}
