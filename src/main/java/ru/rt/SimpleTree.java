package ru.rt;

public class SimpleTree implements Tree{
    protected Node root = null;
    protected int size = 0;

    @Override
    public void insert(int x) {
        root = insertRecursive(root, x);
    }

    protected Node insertRecursive(Node current, int key) {
        if (current == null) {
            this.size++;
            return new Node(key);
        }
        if (key < current.key) {
            current.left = insertRecursive(current.left, key);
        } else if (key > current.key) {
            current.right = insertRecursive(current.right, key);
        }

        return current;
    }

    @Override
    public boolean search(int x) {
        return searchRecursive(root,x);
    }

    private boolean searchRecursive(Node current, int key){
        if (current == null) {
            return false;
        }
        if (key == current.key) {
            return true;
        }
        return key < current.key
                ? searchRecursive(current.left, key)
                : searchRecursive(current.right, key);
    }

    @Override
    public void remove(int x) {
        root = removeRecursive(root, x);
    }

    protected Node removeRecursive(Node current, int key) {
        if (current == null) {
            return null;
        }

        if (key == current.key) {
            //если 0 детей
            if (current.left == null && current.right == null) {
                this.size--;
                return null;
            }

            //если 1 детей
            if (current.right == null) {
                this.size--;
                return current.left;
            }
            if (current.left == null) {
                this.size--;
                return current.right;
            }

            //если 2 детей
            int smallestValue = findSmallestValue(current.right);
            current.key = smallestValue;
            current.right = removeRecursive(current.right, smallestValue);
            return current;
        }
        if (key < current.key) {
            current.left = removeRecursive(current.left, key);
            return current;
        }
        current.right = removeRecursive(current.right, key);
        return current;
    }

    private int findSmallestValue(Node root) {
        return root.left == null ? root.key : findSmallestValue(root.left);
    }
}
