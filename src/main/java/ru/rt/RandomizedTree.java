package ru.rt;

import java.util.Random;

public class RandomizedTree extends BalancedTree {
    private Random rand = new Random();

    protected Node insertRoot(Node current, int key) {
        if (current == null) {
            this.size++;
            return new Node(key);
        }
        if (key < current.key) {
            current.left = insertRoot(current.left, key);
            return smallRightRotation(current);
        } else {
            current.right = insertRoot(current.right, key);
            return smallLeftRotation(current);
        }
    }

    @Override
    protected Node insertRecursive(Node current, int key) {
        if (current == null) {
            this.size++;
            return new Node(key);
        }
        if ((rand.nextInt() * current.height) % (current.height + 1) == 0){
            return insertRoot(current, key);
        }
        if (key < current.key) {
            current.left = insertRecursive(current.left, key);
        } else if (key > current.key) {
            current.right = insertRecursive(current.right, key);
        }
        changeHeight(current);
        return current;
    }

    protected Node join(Node p, Node q) {
        if (p == null) return q;
        if (q == null) return p;
        if (rand.nextInt() % (p.height + q.height + 1) < p.height) {
            p.right = join(p.right, q);
            changeHeight(p);
            return p;
        } else {
            q.left = join(p, q.left);
            changeHeight(q);
            return q;
        }
    }

    @Override
    protected Node removeRecursive(Node current, int key) {
        if (current == null) {
            return current;
        }
        if (current.key == key) {
            Node newNode = join(current.left, current.right);
            this.size--;
            return newNode;
        } else if (key < current.key) {
            current.left = removeRecursive(current.left, key);
        } else {
            current.right = removeRecursive(current.right, key);
        }
        return current;
    }
}
