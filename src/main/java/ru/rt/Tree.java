package ru.rt;

public interface Tree {
    void insert(int x);
    boolean search(int x);
    void remove(int x);
}
