package ru.rt;

public class Node {
    Node left;
    Node right;
    int key;
    int height;

    public Node(int key){
        this(key, 0, null, null);
    }

    public Node(int key, int height){
        this(key, height, null, null);
    }

    public Node(int key, int height, Node left, Node right){
        this.left = left;
        this.right = right;
        this.key = key;
        this.height = height;
    }

}
