package ru.rt;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Consumer;

public class Main {
    public static final int size = 1_000_000_0;

    public static void main(String[] args) throws Exception {
        List<Integer> arr = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            arr.add(i);
        }

//        Collections.shuffle(arr);
        Tree tree = new RandomizedTree();
        String message = tree.getClass().getName() + " добавление в случайном порядке ";
        timer(message,()->{
            for (int i = 0; i < size; i++) {
                tree.insert(arr.get(i));
            }
        });
        message = tree.getClass().getName() + " поиск в случайном порядке ";
        timer(message,()->{
            for (int i = 0; i < size; i+=10) {
                tree.search(arr.get(i));
            }
        });
        message = tree.getClass().getName() + " удаление в случайном порядке ";
        timer(message,()->{
            for (int i = 0; i < size; i+=10) {
                tree.remove(arr.get(i));
            }
        });
        message = tree.getClass().getName() + " поиск в случайном порядке ";
        timer(message,()->{
            for (int i = 0; i < size; i+=10) {
                tree.search(arr.get(i));
            }
        });
    }

    private static void timer(String message, Runnable func){
        long start = System.currentTimeMillis();
        func.run();
        long now = System.currentTimeMillis();
        System.out.println(message + " " + (now - start) + " ms.");
    }

}
