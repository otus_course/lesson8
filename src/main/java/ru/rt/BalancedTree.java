package ru.rt;

public class BalancedTree extends SimpleTree {
    protected Node smallRightRotation(Node n) {
        Node tmp = n.left;
        n.left = tmp.right;
        tmp.right = n;
        changeHeight(n);
        changeHeight(tmp);
        return tmp;
    }

    protected Node smallLeftRotation(Node n) {
        Node tmp = n.right;
        n.right = tmp.left;
        tmp.left = n;
        changeHeight(n);
        changeHeight(tmp);
        return tmp;

    }

    protected Node bigLeftRotation(Node n) {
        n.right = smallRightRotation(n.right);
        return smallLeftRotation(n);
    }

    protected Node bigRightRotation(Node n) {
        n.left = smallLeftRotation(n.left);
        return smallRightRotation(n);

    }

    protected Node rebalance(Node n) {
        changeHeight(n);
        if (calcDiff(n) == 2) {
            if (calcDiff(n.right) < 0) {
                return bigLeftRotation(n);
            } else {
                return smallLeftRotation(n);
            }
        }
        if (calcDiff(n) == -2) {
            if (calcDiff(n.left) > 0) {
                return bigRightRotation(n);
            } else {
                return smallRightRotation(n);
            }
        }
        return n;

    }

    private int calcDiff(Node n) {
        if(n == null)return 0;
        int hl = (n.left == null) ? 0 : n.left.height;
        int hr = (n.right == null) ? 0 : n.right.height;
        return hr - hl;
    }

    protected void changeHeight(Node n) {
        int hl = (n.left == null) ? 0 : n.left.height;
        int hr = (n.right == null) ? 0 : n.right.height;
        n.height = (hl > hr ? hl : hr) + 1;
    }

    @Override
    protected Node insertRecursive(Node current, int key) {
        return rebalance(super.insertRecursive(current,key));
    }

    @Override
    protected Node removeRecursive(Node current, int key) {
        super.removeRecursive(current,key);
        return rebalance(current);
    }
}
