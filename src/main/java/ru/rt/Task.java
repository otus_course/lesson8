package ru.rt;

public interface Task {
    String run(String[] data);
}
